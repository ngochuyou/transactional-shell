/**
 * Wincal-X Project TransactionManagementTest.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction;

import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaDelete;
import jakarta.transaction.Transactional;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import transaction.domain.Deposit;

/**
 * TransactionManagementTest
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@TestPropertySource("classpath:application-test.yml")
@ActiveProfiles("test")
class TransactionManagementTest {

  private static final Logger logger = LoggerFactory.getLogger(TransactionManagementTest.class);

  @Autowired
  ApplicationContext applicationContext;

  @BeforeAll
  public static void beforeEach() {
    final SessionFactory sessionFactory = getSessionFactory();
    final Session session = sessionFactory.getCurrentSession();
    final CriteriaBuilder builder = sessionFactory.getCriteriaBuilder();
    final CriteriaDelete<Deposit> criteriaDelete = builder.createCriteriaDelete(Deposit.class);

    criteriaDelete.where(builder.conjunction()).from(Deposit.class);
  }

  private Session getCurrentSession() {
    return getSessionFactory().getCurrentSession();
  }

  private SessionFactory getSessionFactory() {
    return applicationContext.getBean(SessionFactory.class);
  }

  @Test
  @Transactional
  void test() {
    final Session session = getCurrentSession();

    logger.info(session.getTransaction().toString());
  }

}
