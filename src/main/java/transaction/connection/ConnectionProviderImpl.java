/**
 * Wincal-X Project ConnectionProviderImpl.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.connection;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;
import javax.sql.DataSource;
import org.springframework.stereotype.Component;

/**
 * ConnectionProviderImpl
 */
@Component("connectionProviderImpl")
public class ConnectionProviderImpl implements ConnectionProvider {

  private Supplier<AtomicReference<Connection>> lazyConnectionSupplier;

  public ConnectionProviderImpl(DataSource dataSource) {
    lazyConnectionSupplier = () -> {
      final AtomicReference<Connection> ref = new AtomicReference<>(createConnection(dataSource));

      lazyConnectionSupplier = () -> ref;

      return ref;
    };
  }

  private static Connection createConnection(DataSource dataSource) {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public Connection getConnection() {
    return lazyConnectionSupplier.get().get();
  }

}
