/**
 * Wincal-X Project ConnectionProvider.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.connection;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * ConnectionProvider
 */
public interface ConnectionProvider {

  Connection getConnection() throws SQLException;

}
