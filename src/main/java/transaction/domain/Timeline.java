package transaction.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import java.math.BigInteger;
import java.time.Instant;

@Entity
@Table(name = "timelines")
public class Timeline {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private BigInteger id;

  @Column(name = "in_timestamp", nullable = false, updatable = false)
  private Instant inTimestamp;

  @Column(name = "out_timestamp")
  private Instant outTimestamp;

  @Column(nullable = false)
  private String username;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public BigInteger getId() {
    return id;
  }

  public void setId(BigInteger id) {
    this.id = id;
  }

  public Instant getInTimestamp() {
    return inTimestamp;
  }

  public void setInTimestamp(Instant inTimestamp) {
    this.inTimestamp = inTimestamp;
  }

  public Instant getOutTimestamp() {
    return outTimestamp;
  }

  public void setOutTimestamp(Instant outTimestamp) {
    this.outTimestamp = outTimestamp;
  }
}
