/**
 * Wincal-X Project Session.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.session;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Savepoint;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import javax.sql.DataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import transaction.connection.ConnectionProvider;

/**
 * Session
 */
@Component
public class Session implements ConnectionProvider {

  private final DataSource dataSource;
  private final AtomicReference<Connection> connectionRef = new AtomicReference<>();

  private final Map<String, Savepoint> savePointsMap = new ConcurrentHashMap<>();

  public Session(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public String open() throws SQLException {
    if (Objects.nonNull(connectionRef.get())) {
      return "A connection was already established";
    }

    connectionRef.set(dataSource.getConnection());
    return "Connection opened";
  }

  public void close() throws SQLException {
    connectionRef.getAndSet(null).close();
  }

  public int begin(Integer isolationLevel) throws SQLException {
    final Connection connection = connectionRef.get();

    connection.setAutoCommit(false);

    if (Objects.nonNull(isolationLevel)) {
      connection.setTransactionIsolation(isolationLevel);
    }

    return connection.getTransactionIsolation();
  }

  public void commit() throws SQLException {
    connectionRef.get().commit();
  }

  public void save(String savepointName) throws SQLException {
    if (savePointsMap.containsKey(savepointName)) {
      throw new IllegalArgumentException("Savepoint already existed");
    }

    savePointsMap.put(savepointName, connectionRef.get().setSavepoint(savepointName));
  }

  private void assertSavepointNonExistence(String savepointName) {
    if (savePointsMap.containsKey(savepointName)) {
      return;
    }

    throw new IllegalArgumentException("Savepoint not found");
  }

  public void release(String savepointName) {
    assertSavepointNonExistence(savepointName);
    savePointsMap.remove(savepointName);
  }

  public void rollback(String savepointName) throws SQLException {
    if (!StringUtils.hasLength(savepointName)) {
      connectionRef.get().rollback();
      return;
    }

    assertSavepointNonExistence(savepointName);
    connectionRef.get().rollback(savePointsMap.get(savepointName));
  }

  @Override
  public Connection getConnection() {
    return connectionRef.get();
  }

}
