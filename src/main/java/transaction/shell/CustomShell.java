package transaction.shell;

import org.jline.terminal.Terminal;
import org.springframework.shell.InputProvider;
import org.springframework.shell.ResultHandlerService;
import org.springframework.shell.Shell;
import org.springframework.shell.command.CommandCatalog;
import org.springframework.shell.context.ShellContext;
import org.springframework.shell.exit.ExitCodeMappings;

public class CustomShell extends Shell {

  private final ResultHandlerService resultHandlerService;

  public CustomShell(ResultHandlerService resultHandlerService,
      CommandCatalog commandRegistry,
      Terminal terminal,
      ShellContext shellContext,
      ExitCodeMappings exitCodeMappings) {
    super(resultHandlerService, commandRegistry, terminal, shellContext, exitCodeMappings);
    this.resultHandlerService = resultHandlerService;
  }

  @Override
  public void run(InputProvider inputProvider) {
    try {
      resultHandlerService.handle(evaluate(inputProvider.readInput()));
    } catch (Exception e) {
      resultHandlerService.handle(e);
    }
  }
}
