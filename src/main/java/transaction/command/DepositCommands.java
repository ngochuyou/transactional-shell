/**
 * Wincal-X Project DepositCommands.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import transaction.command.strategy.CommandStrategyType;
import transaction.domain.Deposit;

/**
 * DepositCommands
 */
@ShellComponent
public class DepositCommands extends AbstractStrategicCommands {

  public DepositCommands(BeanFactory beanFactory) {
    super(beanFactory);
  }

  private static Deposit createNewDeposit(BigDecimal amount) {
    final Deposit newDeposit = new Deposit();

    newDeposit.setAmount(amount);
    return newDeposit;
  }

  private static List<String> resolveIds(String depositIds) {
    return Stream.of(depositIds.split(" *, *")).toList();
  }

  private static String render(Deposit deposit) {
    return String.format("%s \t %s \t %s \t %s",
        deposit.getId(),
        deposit.getAmount(),
        deposit.getCreatedTimestamp(),
        deposit.getUpdatedTimestamp());
  }

  @ShellMethod(key = "save deposit")
  public String deposit(
      @ShellOption BigDecimal amount,
      @ShellOption String type) {
    final Deposit newDeposit = (Deposit) insertStrategies.get(CommandStrategyType.from(type))
        .apply(createNewDeposit(amount));

    return String.format("Created one new deposit: %n%s \t %s \t %s \t %s%n",
        newDeposit.getId(),
        newDeposit.getAmount(),
        newDeposit.getCreatedTimestamp(),
        newDeposit.getUpdatedTimestamp());
  }

  @ShellMethod(key = "get deposit")
  public String queryDeposit(
      @ShellOption BigInteger depositId,
      @ShellOption String type) {
    final Deposit deposit = (Deposit) singleSelectStrategies.get(CommandStrategyType.from(type))
        .apply(depositId, Deposit.class);

    if (Objects.isNull(deposit)) {
      throw new IllegalArgumentException(
          String.format("%s#%s not found", Deposit.class.getSimpleName(), depositId.toString()));
    }

    return String.format("Found one result: %n%s%n", render(deposit));
  }

  @SuppressWarnings("unchecked")
  @ShellMethod(key = "m-get deposit")
  public String queryDeposit(
      @ShellOption String depositIds,
      @ShellOption String type) {
    final List<Deposit> deposits = (List<Deposit>) multiSelectStrategies.get(
            CommandStrategyType.from(type))
        .apply(resolveIds(depositIds), Deposit.class);

    return String.format("Found %d results: %n%s%n",
        deposits.size(),
        deposits.stream().map(DepositCommands::render)
            .collect(Collectors.joining("\n")));
  }

  @ShellMethod(key = "lock deposit")
  public String lock(
      @ShellOption String ids,
      @ShellOption String type) {
    return Optional.ofNullable(lockStrategies.get(CommandStrategyType.from(type))
            .apply(resolveIds(ids), Deposit.class))
        .map(lockId -> String.format("Locked a record: %s", lockId))
        .orElse(String.format("Unable to obtain lock %s", ids));
  }

}
