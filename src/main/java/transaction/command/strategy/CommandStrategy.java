/**
 * Wincal-X Project CommandStrategy.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command.strategy;

import java.io.Serializable;
import java.util.List;

/**
 * CommandStrategy
 */
public interface CommandStrategy {

  <T> T save(T entity);

  <T> T update(T entity);

  <S extends Serializable, T> T find(S id, Class<T> type);

  <S extends Serializable, T> List<T> findAll(List<S> ids, Class<T> type);

  <S extends Serializable, T> List<S> lock(List<S> ids, Class<T> type);

}
