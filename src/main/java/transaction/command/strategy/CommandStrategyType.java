/**
 * Wincal-X Project CommandStrategyType.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command.strategy;

import java.util.stream.Stream;

/**
 * CommandStrategyType
 */
public enum CommandStrategyType {

  JPA, SIMPLE, CACHED, SESSION;

  public static CommandStrategyType from(String stringValue) {
    return Stream.of(CommandStrategyType.values())
        .filter(value -> value.name().equalsIgnoreCase(stringValue))
        .findAny()
        .orElseThrow(
            () -> new IllegalArgumentException(String.format("Unknown strategy: %s", stringValue)));
  }

}
