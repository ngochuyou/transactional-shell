/**
 * Wincal-X Project CachedConnectionsCommandStrategy.java Copyright © ALMEX Inc. All rights
 * reserved.
 */
package transaction.command.strategy.impl;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import transaction.connection.ConnectionProvider;

/**
 * CachedConnectionsCommandStrategy
 */
@Component("cachedConnectionsCommandStrategy")
public class CachedConnectionsCommandStrategy extends AbstractJdbcCommandStrategy {

  private final ConnectionProvider connectionProvider;

  public CachedConnectionsCommandStrategy(SessionFactory sessionFactory,
      @Qualifier("connectionProviderImpl") ConnectionProvider connectionProvider,
      DataSource dataSource) {
    super(sessionFactory, connection -> {
    }, dataSource);
    this.connectionProvider = connectionProvider;
  }

  @Override
  protected Connection getConnection() {
    try {
      return connectionProvider.getConnection();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

}
