/**
 * Wincal-X Project SessionSupportedCommandStrategy.java Copyright © ALMEX Inc. All rights
 * reserved.
 */
package transaction.command.strategy.impl;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;
import transaction.connection.ConnectionProvider;
import transaction.session.Session;

/**
 * SessionSupportedCommandStrategy
 */
@Component("sessionSupportedCommandStrategy")
public class SessionSupportedCommandStrategy extends AbstractJdbcCommandStrategy {

  private final ConnectionProvider connectionProvider;

  public SessionSupportedCommandStrategy(SessionFactory sessionFactory,
      Session connectionProvider, DataSource dataSource) {
    super(sessionFactory, connection -> {
    }, dataSource);
    this.connectionProvider = connectionProvider;
  }

  @Override
  protected Connection getConnection() {
    try {
      return connectionProvider.getConnection();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }
}
