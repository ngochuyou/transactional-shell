/**
 * Wincal-X Project AbstractJdbcCommandStrategy.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command.strategy.impl;

import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.id.Assigned;
import org.hibernate.id.CompositeNestedGeneratedValueGenerator;
import org.hibernate.id.IdentifierGenerator;
import org.hibernate.id.enhanced.DatabaseStructure;
import org.hibernate.id.enhanced.SequenceStructure;
import org.hibernate.id.enhanced.SequenceStyleGenerator;
import org.hibernate.id.enhanced.TableStructure;
import org.hibernate.metamodel.MappingMetamodel;
import org.hibernate.metamodel.mapping.AttributeMapping;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.tuple.InMemoryValueGenerationStrategy;
import org.hibernate.tuple.ValueGenerator;
import org.hibernate.tuple.entity.EntityMetamodel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import transaction.command.strategy.CommandStrategy;
import transaction.declaration.Declarations;
import transaction.function.HandledFunction;

/**
 * AbstractJdbcCommandStrategy
 */
public abstract class AbstractJdbcCommandStrategy implements CommandStrategy {

  @SuppressWarnings("rawtypes")
  private static final Map<Class, Map<Class, Function<Object, Object>>> EXPLICIT_TYPE_CONVERTERS = Map.of(
      LocalDateTime.class, Map.of(
          Timestamp.class, value -> ((Timestamp) value).toLocalDateTime()),
      Instant.class, Map.of(
          LocalDateTime.class, value -> ((LocalDateTime) value).toInstant(ZoneOffset.UTC))
  );
  private final MappingMetamodel metamodel;
  private final PostExecutionConnectionHandler connectionHandler;
  private final DataSource dataSource;

  protected AbstractJdbcCommandStrategy(SessionFactory sessionFactory, DataSource dataSource) {
    this(sessionFactory, AutoClosedConnectionHandler.INSTANCE, dataSource);
  }

  protected AbstractJdbcCommandStrategy(SessionFactory sessionFactory,
      PostExecutionConnectionHandler connectionHandler,
      DataSource dataSource) {
    final SessionFactoryImplementor sfi = sessionFactory.unwrap(SessionFactoryImplementor.class);

    this.metamodel = sfi.getMappingMetamodel();
    this.connectionHandler = connectionHandler;
    this.dataSource = dataSource;
  }

  private static <T> T setValues(ResultSet resultSet, EntityPersister persister, T instance)
      throws SQLException {
    final int span = persister.getPropertyNames().length;

    persister.setIdentifier(instance,
        persister.getIdentifierMapping()
            .getJavaType()
            .fromString(Optional.ofNullable(resultSet.getObject(1))
                .map(Object::toString)
                .orElse(null)),
        null);

    for (int i = 0; i < span; i++) {
      final Object value = resultSet.getObject(i + 2);
      final AttributeMapping attributeMapping = persister.getAttributeMapping(i);

      if (Objects.isNull(value)) {
        attributeMapping.setValue(instance, null);
        continue;
      }

      persister.setValue(instance, i, convert(value, attributeMapping));
    }

    return instance;
  }

  @SuppressWarnings("unchecked")
  private static <T> T hydrate(ResultSet resultSet, EntityPersister persister)
      throws InstantiationException, IllegalAccessException, InvocationTargetException,
      NoSuchMethodException, SQLException {
    final T instance = (T) persister.getRepresentationStrategy()
        .getMappedJavaType()
        .getJavaTypeClass()
        .getDeclaredConstructor()
        .newInstance();

    return setValues(resultSet, persister, instance);
  }

  private static Object convert(Object value, AttributeMapping attributeMapping) {
    final Class<?> mappedType = attributeMapping.getJavaType().getJavaTypeClass();

    if (!EXPLICIT_TYPE_CONVERTERS.containsKey(mappedType)) {
      return fromString(value, attributeMapping);
    }

    final Class<?> valueType = value.getClass();

    if (!EXPLICIT_TYPE_CONVERTERS.get(mappedType).containsKey(valueType)) {
      return fromString(value, attributeMapping);
    }

    return EXPLICIT_TYPE_CONVERTERS.get(mappedType).get(valueType).apply(value);
  }

  private static Object fromString(Object value, AttributeMapping attributeMapping) {
    return attributeMapping.getJavaType()
        .fromString(Optional.ofNullable(value).map(Object::toString).orElse(null));
  }

  private <T> List<T> hydrate(ResultSet resultSet, Class<T> type)
      throws SQLException, NoSuchMethodException, InvocationTargetException,
      InstantiationException, IllegalAccessException {
    final EntityPersister persister = metamodel.getEntityDescriptor(type);
    final List<T> result = new ArrayList<>();

    while (resultSet.next()) {
      result.add(hydrate(resultSet, persister));
    }

    return result;
  }

  @Override
  public <T> T save(T entity) {
    final Connection connection = getConnection();

    try (final PreparedStatement statement = prepareInsertStatement(entity, connection)) {
      final int updates = statement.executeUpdate();

      if (updates == 0) {
        throw new IllegalStateException("No updates were made");
      }

      return entity;
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      connectionHandler.handleConnection(connection);
    }
  }

  @Override
  public <T> T update(T entity) {
    final Connection connection = getConnection();

    try (final PreparedStatement statement = new UpdateStatementPreparer<>(connection,
        metamodel.getEntityDescriptor(entity.getClass()),
        entity).prepare()) {
      final int updates = statement.executeUpdate();

      if (updates == 0) {
        throw new IllegalStateException("No updates were made");
      }

      return entity;
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      connectionHandler.handleConnection(connection);
    }
  }

  @Override
  public <S extends Serializable, T> T find(S id, Class<T> type) {
    final Connection connection = getConnection();

    try (final PreparedStatement statement = prepareSelectStatement(List.of(id), type,
        connection)) {
      return hydrate(statement.executeQuery(), type).stream()
          .findFirst().orElse(null);
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      connectionHandler.handleConnection(connection);
    }
  }

  @Override
  public <S extends Serializable, T> List<T> findAll(List<S> ids, Class<T> type) {
    final Connection connection = getConnection();

    try (final PreparedStatement statement = prepareSelectStatement(ids,
        type, connection)) {
      return hydrate(statement.executeQuery(), type);
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      connectionHandler.handleConnection(connection);
    }
  }

  @Override
  public <S extends Serializable, T> List<S> lock(List<S> ids, Class<T> type) {
    final Connection connection = getConnection();

    try (final PreparedStatement statement = prepareSelectStatement(ids,
        List.of(metamodel.getEntityDescriptor(type).getIdentifierPropertyName()),
        type, connection, true)) {
      if (statement.executeQuery().next()) {
        return ids;
      }

      return Collections.emptyList();
    } catch (Exception e) {
      throw new IllegalStateException(e);
    } finally {
      connectionHandler.handleConnection(connection);
    }
  }

  private <T> PreparedStatement prepareInsertStatement(T entity, Connection connection)
      throws SQLException {
    return new InsertStatementPreparer<>(connection,
        dataSource, metamodel.getEntityDescriptor(entity.getClass()), entity)
        .prepare();
  }


  private <S extends Serializable, T> PreparedStatement prepareSelectStatement(List<S> ids,
      Class<T> type,
      Connection connection) throws SQLException {
    return prepareSelectStatement(ids, Collections.emptyList(), type, connection, false);
  }

  private <S extends Serializable, T> PreparedStatement prepareSelectStatement(List<S> ids,
      List<String> columnNames,
      Class<T> type,
      Connection connection,
      boolean lock) throws SQLException {
    return new SelectStatementPreparer<S>(connection, metamodel.getEntityDescriptor(type))
        .prepare(ids, columnNames, lock);
  }

  protected abstract Connection getConnection();

  protected interface PostExecutionConnectionHandler {

    void handleConnection(Connection connection);

  }

  private static class SelectStatementPreparer<S extends Serializable> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SelectStatementPreparer.class);

    private final Connection connection;

    private final AbstractEntityPersister persister;

    public SelectStatementPreparer(Connection connection, EntityPersister persister) {
      this.connection = connection;
      this.persister = (AbstractEntityPersister) persister;
    }

    private static SelectStatementBuilder doSelect(SelectStatementBuilder builder,
        List<String> columnNames) {
      return CollectionUtils.isEmpty(columnNames)
          ? builder.select()
          : builder.select(columnNames);
    }

    private static SelectStatementBuilder doWhere(
        SelectStatementBuilder builder,
        int size) {
      return size == 1
          ? builder.where()
          : builder.where(size);
    }

    private static UnaryOperator<SelectStatementBuilder> getLockInterpreter(
        boolean lock) {
      return lock
          ? SelectStatementBuilder::lock
          : UnaryOperator.identity();
    }

    public PreparedStatement prepare(List<S> ids, List<String> columnNames, boolean lock)
        throws SQLException {
      final int size = ids.size();
      final PreparedStatement preparedStatement = Declarations.of(persister)
          .map(SelectStatementBuilder::from)
          .second(columnNames)
          .map(SelectStatementPreparer::doSelect)
          .second(ids.size())
          .map(SelectStatementPreparer::doWhere)
          .map(getLockInterpreter(lock))
          .map(SelectStatementBuilder::build)
          .tryMap(connection::prepareStatement)
          .get();

      for (int i = 0; i < size; i++) {
        preparedStatement.setObject(i + 1, ids.get(i));
      }

      LOGGER.debug("\n{}", preparedStatement);

      return preparedStatement;
    }

    private static class SelectStatementBuilder {

      private final AbstractEntityPersister persister;

      private final String fromClause;
      private String selectClause;
      private String whereClause;
      private String lockClause;

      private SelectStatementBuilder(AbstractEntityPersister persister) {
        this.persister = persister;
        fromClause = String.format("FROM %s", persister.getTableName());
      }

      public static SelectStatementBuilder from(AbstractEntityPersister persister) {
        return new SelectStatementBuilder(persister);
      }

      public SelectStatementBuilder select() {
        selectClause = String.format("SELECT %s", getColumnNames());
        return this;
      }

      public SelectStatementBuilder select(List<String> columnNames) {
        selectClause = String.format("SELECT %s", getColumnNames(columnNames));
        return this;
      }

      public SelectStatementBuilder where() {
        whereClause = String.format("WHERE %s = ?", persister.getIdentifierPropertyName());
        return this;
      }

      public SelectStatementBuilder where(int size) {
        whereClause = String.format("WHERE %s IN (%s)",
            persister.getIdentifierPropertyName(),
            IntStream.range(0, size).mapToObj(i -> "?").collect(Collectors.joining(", ")));
        return this;
      }

      public SelectStatementBuilder lock() {
        lockClause = "FOR UPDATE";
        return this;
      }

      public String build() {
        return Stream.of(selectClause, fromClause, whereClause, lockClause)
            .filter(StringUtils::hasLength)
            .collect(Collectors.joining(" "));
      }

      private String getColumnNames() {
        return getColumnNames(Stream.concat(Stream.of(persister.getIdentifierColumnNames()[0]),
                Stream.of(persister.getPropertyNames())
                    .map(persister::getPropertyColumnNames)
                    .map(columns -> columns[0]))
            .toList());
      }

      private String getColumnNames(List<String> columnNames) {
        return String.join(", ", columnNames);
      }
    }

  }

  private static class InsertStatementPreparer<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(InsertStatementPreparer.class);

    private final Connection connection;
    private final DataSource dataSource;
    private final AbstractEntityPersister persister;
    private final EntityMetamodel metamodel;
    private final Map<Class<? extends DatabaseStructure>, HandledFunction<DatabaseStructure, Object, SQLException>>
        identifierGenerator = Map.of(
        TableStructure.class, this::generateIdentifierFromTableStructure,
        SequenceStructure.class, this::generateIdentifierFromSequenceStructure);
    private final T entity;

    public InsertStatementPreparer(Connection connection, DataSource dataSource,
        EntityPersister persister, T entity) {
      this.connection = connection;
      this.dataSource = dataSource;
      this.persister = (AbstractEntityPersister) persister;
      metamodel = persister.getEntityMetamodel();
      this.entity = entity;
    }

    private static void updateIdentifier(Connection autoIncrementConnection,
        TableStructure databaseStructure, Long identifier) throws SQLException {
      final Long newIdentifier = identifier + databaseStructure.getIncrementSize();
      try (final PreparedStatement statement = autoIncrementConnection.prepareStatement(
          databaseStructure.getAllSqlForTests()[1])) {

        statement.setObject(1, newIdentifier);
        statement.setObject(2, identifier);

        LOGGER.debug("\n{}", statement);

        if (statement.executeUpdate() != 1) {
          throw new IllegalStateException("Unable to update sequence value");
        }
      }
    }

    @SuppressWarnings("squid:S2095")
    public PreparedStatement prepare() throws SQLException {
      final PreparedStatement preparedStatement = connection.prepareStatement(
          persister.getSQLInsertStrings()[0]);
      final Object[] values = persister.getValues(entity);
      final int length = values.length;

      for (int i = 0; i < length; i++) {
        setValue(preparedStatement, values, i);
      }

      if (isIdentifierAutoGenerated()) {
        final IdentifierGenerator generator = persister.getIdentifierGenerator();

        if (!(generator instanceof SequenceStyleGenerator sequenceStyleGenerator)) {
          return preparedStatement;
        }

        final Object identifier = getIdentifier(sequenceStyleGenerator);

        preparedStatement.setObject(length + 1, identifier);
        persister.getIdentifierMapping().setIdentifier(
            entity,
            persister.getIdentifierMapping()
                .getJdbcMappings()
                .get(0)
                .getJavaTypeDescriptor()
                .fromString(identifier.toString()),
            null);
      }

      LOGGER.debug("\n{}", preparedStatement);

      return preparedStatement;
    }

    private Object getIdentifier(SequenceStyleGenerator generator) throws SQLException {
      final DatabaseStructure databaseStructure = generator.getDatabaseStructure();

      return identifierGenerator.get(databaseStructure.getClass()).apply(databaseStructure);
    }

    private Object generateIdentifierFromSequenceStructure(DatabaseStructure structure)
        throws SQLException {
      return getIdentifier(structure, connection);
    }

    @SuppressWarnings("deprecation")
    private Object getIdentifier(DatabaseStructure databaseStructure, Connection connection)
        throws SQLException {
      final PreparedStatement statement = connection.prepareStatement(
          databaseStructure.getAllSqlForTests()[0]);

      LOGGER.debug("\n{}", statement);

      final ResultSet resultSet = statement.executeQuery();

      if (resultSet.next()) {
        return resultSet.getObject(1);
      }

      throw new IllegalStateException("Unable to retrieve an identifier");
    }

    private Object generateIdentifierFromTableStructure(DatabaseStructure structure)
        throws SQLException {
      try (final Connection autoIncrementConnection = prepareAutoIncrementConnection()) {
        final TableStructure databaseStructure = (TableStructure) structure;
        final Object identifier = getIdentifier(databaseStructure, autoIncrementConnection);

        updateIdentifier(autoIncrementConnection, databaseStructure, (Long) identifier);

        autoIncrementConnection.commit();

        return identifier;
      }
    }

    @SuppressWarnings("squid:S2095")
    private Connection prepareAutoIncrementConnection() throws SQLException {
      Connection aiConnection = dataSource.getConnection();

      aiConnection.setAutoCommit(false);
      aiConnection.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);

      return aiConnection;
    }

    private void setValue(PreparedStatement preparedStatement, Object[] values, int i)
        throws SQLException {
      final InMemoryValueGenerationStrategy propertyGenerator =
          (InMemoryValueGenerationStrategy) getPropertyGenerator(i);
      final ValueGenerator<?> valueGenerator = Optional.ofNullable(propertyGenerator)
          .map(InMemoryValueGenerationStrategy::getValueGenerator)
          .orElse(null);

      if (Objects.isNull(valueGenerator)) {
        preparedStatement.setObject(i + 1, values[i]);
        return;
      }

      final Object value = valueGenerator.generateValue(null, entity);

      preparedStatement.setObject(i + 1, value);
      persister.setValue(entity, i, value);
    }

    private Object getPropertyGenerator(int i) {
      return Stream.of(metamodel.getInMemoryValueGenerationStrategies()[i],
              metamodel.getInDatabaseValueGenerationStrategies()[i])
          .filter(Objects::nonNull)
          .findAny()
          .orElse(null);
    }

    private boolean isIdentifierAutoGenerated() {
      final IdentifierGenerator generator = persister.getIdentifierGenerator();

      return !(generator instanceof CompositeNestedGeneratedValueGenerator
          || generator instanceof Assigned);
    }


  }

  private static class UpdateStatementPreparer<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateStatementPreparer.class);

    private final Connection connection;
    private final AbstractEntityPersister persister;
    private final T entity;

    private UpdateStatementPreparer(Connection connection, EntityPersister persister,
        T entity) {
      this.connection = connection;
      this.persister = (AbstractEntityPersister) persister;
      this.entity = entity;
    }

    private static int getStatementIndex(int index) {
      return index + 1;
    }

    @SuppressWarnings("squid:S2095")
    public PreparedStatement prepare() throws SQLException {
      final PreparedStatement preparedStatement = connection.prepareStatement(
          persister.getSQLUpdateStrings()[0]);
      final Object[] values = Stream.of(persister.getPropertyNames())
          .map(persister::getPropertyIndex)
          .filter(index -> persister.getPropertyColumnUpdateable()[index][0])
          .map(index -> persister.getValue(entity, index))
          .toArray();

      for (int i = 0; i < values.length; i++) {
        preparedStatement.setObject(getStatementIndex(i), values[i]);
      }

      preparedStatement.setObject(getStatementIndex(values.length),
          persister.getIdentifierMapping().getIdentifier(entity));

      LOGGER.debug("\n{}", preparedStatement);

      return preparedStatement;
    }

  }

  protected static class AutoClosedConnectionHandler implements PostExecutionConnectionHandler {

    private static final AutoClosedConnectionHandler INSTANCE = new AutoClosedConnectionHandler();

    private AutoClosedConnectionHandler() {
    }

    @Override
    public void handleConnection(Connection connection) {
      try {
        connection.close();
      } catch (SQLException e) {
        throw new IllegalStateException(e);
      }
    }

  }

}
