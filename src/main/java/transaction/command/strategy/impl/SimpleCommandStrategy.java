/**
 * Wincal-X Project SimpleCommandStrategy.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command.strategy.impl;

import java.sql.Connection;
import java.sql.SQLException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Component;

/**
 * SimpleCommandStrategy
 */
@Component("simpleCommandStrategy")
public class SimpleCommandStrategy extends AbstractJdbcCommandStrategy {

  private final DataSource dataSource;

  public SimpleCommandStrategy(SessionFactory sessionFactory,
      DataSource dataSource) {
    super(sessionFactory, dataSource);
    this.dataSource = dataSource;
  }

  @Override
  protected Connection getConnection() {
    try {
      return dataSource.getConnection();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }
}
