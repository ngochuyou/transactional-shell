/**
 * Wincal-X Project JpaCommandStrategy.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command.strategy.impl;

import java.io.Serializable;
import java.util.List;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.query.Query;
import org.hibernate.query.criteria.HibernateCriteriaBuilder;
import org.hibernate.query.criteria.JpaCriteriaQuery;
import org.hibernate.query.criteria.JpaRoot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import transaction.command.strategy.CommandStrategy;
import transaction.declaration.Declarations;

/**
 * JpaCommandStrategy
 */
@Component("jpaCommandStrategy")
public class JpaCommandStrategy implements CommandStrategy {

  private static final Logger logger = LoggerFactory.getLogger(JpaCommandStrategy.class);

  private final SessionFactory sessionFactory;

  public JpaCommandStrategy(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  private static <T> String locateIdentifierName(Class<T> type, Session session) {
    return session.getSessionFactory().unwrap(SessionFactoryImplementor.class).getMappingMetamodel()
        .getEntityDescriptor(type).getIdentifierPropertyName();
  }

  @Override
  @Transactional
  public <T> T save(T entity) {
    sessionFactory.getCurrentSession().persist(entity);
    return entity;
  }

  @Override
  public <T> T update(T entity) {
    return save(entity);
  }

  @Override
  @Transactional(readOnly = true)
  public <S extends Serializable, T> T find(S id, Class<T> type) {
    return sessionFactory.getCurrentSession().find(type, id);
  }

  @Override
  @Transactional(propagation = Propagation.SUPPORTS)
  public <S extends Serializable, T> List<T> findAll(List<S> ids, Class<T> type) {
    final Session session = sessionFactory.getCurrentSession();
    final HibernateCriteriaBuilder cb = session.getCriteriaBuilder();
    final JpaCriteriaQuery<T> cq = cb.createQuery(type);
    final JpaRoot<T> root = cq.from(type);

    return Declarations.of(type, session)
        .map(JpaCommandStrategy::locateIdentifierName)
        .map(root::get)
        .map(cb::in)
        .map(in -> in.value(ids))
        .map(cq::where)
        .map(session::createQuery)
        .consume(query -> logger.info("\n\t{}", query.getQueryString()))
        .map(Query::getResultList)
        .get();
  }

  @Override
  public <S extends Serializable, T> List<S> lock(List<S> ids, Class<T> type) {
    sessionFactory.getCurrentSession().lock(findAll(ids, type), LockMode.PESSIMISTIC_READ);

    return ids;
  }
}
