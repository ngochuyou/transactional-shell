/**
 * Wincal-X Project PipelineCommands.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command;

import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.shell.Input;
import org.springframework.shell.InputProvider;
import org.springframework.shell.Shell;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import transaction.declaration.Declarations;
import transaction.session.Session;
import transaction.shell.CustomShell;

/**
 * PipelineCommands
 */
@ShellComponent
public class SessionCommands {

  private static final Logger LOGGER = LoggerFactory.getLogger(SessionCommands.class);
  private static final String COMMAND_DELIMITER = "\\s*;\\s*";

  private final Session session;
  private final AutowireCapableBeanFactory beanFactory;

  public SessionCommands(Session session, AutowireCapableBeanFactory beanFactory) {
    this.session = session;
    this.beanFactory = beanFactory;
  }

  @ShellMethod(key = "session")
  public String executeUnderSession(
      @ShellOption String commandString) {
    try {
      return Declarations.of(commandString)
          .map(this::split)
          .map(this::intercept)
          .consume(this::log)
          .map(this::input)
          .tryMap(this::run)
          .get();
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

  private List<String> intercept(List<String> rawCommands) {
    return rawCommands.stream().map(rawCommand -> rawCommand.concat(" session")).toList();
  }

  @ShellMethod(key = "open session")
  public String open() {
    try {
      return session.open();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "close session")
  public String close() {
    try {
      session.close();

      return "Connection closed";
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "begin session")
  public String begin(
      @ShellOption(defaultValue = ShellOption.NULL) Integer isolationLevel) {
    try {
      return String.format("Transaction begun, isolation level: %s",
          session.begin(isolationLevel));
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "commit session")
  public String commit() {
    try {
      session.commit();
      return "Committed transaction";
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "save session")
  public String save(
      @ShellOption String savePointName) {
    try {
      session.save(savePointName);
      return String.format("Created new savepoint: %s", savePointName);
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "release session")
  public String release(
      @ShellOption String savePointName) {
    session.release(savePointName);
    return String.format("Released %s", savePointName);
  }

  @ShellMethod(key = "rollback session")
  public String rollback(
      @ShellOption(defaultValue = "") String savePointName) {
    try {
      session.rollback(savePointName);
      return "Transaction rolled back";
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  private List<String> split(String commandPipe) {
    return Stream.of(commandPipe.split(COMMAND_DELIMITER)).toList();
  }

  private void log(List<String> commands) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("\nExecuting under session: \n\t{}", String.join("\n\t", commands));
    }
  }

  private List<InputProvider> input(List<String> commands) {
    return commands.stream()
        .map(commandString -> (InputProvider) () -> (Input) () -> commandString)
        .toList();
  }

  private String run(List<InputProvider> commands) throws Exception {
    final Shell customShell = beanFactory.createBean(CustomShell.class);

    for (final InputProvider command : commands) {
      customShell.run(command);
    }

    return "Successfully executed under session";
  }

}
