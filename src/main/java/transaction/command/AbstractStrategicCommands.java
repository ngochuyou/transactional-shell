/**
 * Wincal-X Project DepositCommands.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command;

import static transaction.command.strategy.CommandStrategyType.CACHED;
import static transaction.command.strategy.CommandStrategyType.JPA;
import static transaction.command.strategy.CommandStrategyType.SESSION;
import static transaction.command.strategy.CommandStrategyType.SIMPLE;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Function;
import org.springframework.beans.factory.BeanFactory;
import transaction.command.strategy.CommandStrategy;
import transaction.command.strategy.CommandStrategyType;

/**
 * DepositCommands
 */
public abstract class AbstractStrategicCommands {

  protected final Map<CommandStrategyType, Function<Object, Object>> insertStrategies;
  protected final Map<CommandStrategyType, Function<Object, Object>> updateStrategies;
  protected final Map<CommandStrategyType, BiFunction<Serializable, Class<?>, Object>> singleSelectStrategies;
  protected final Map<CommandStrategyType, BiFunction<List<? extends Serializable>, Class<?>, List<?>>> multiSelectStrategies;
  protected final Map<CommandStrategyType, BiFunction<List<? extends Serializable>, Class<?>, List<? extends Serializable>>> lockStrategies;

  protected AbstractStrategicCommands(BeanFactory beanFactory) {
    final CommandStrategy simpleStrategy = beanFactory.getBean("simpleCommandStrategy",
        CommandStrategy.class);
    final CommandStrategy jpaStrategy = beanFactory.getBean("jpaCommandStrategy",
        CommandStrategy.class);
    final CommandStrategy cachedConnectionsStrategy = beanFactory.getBean(
        "cachedConnectionsCommandStrategy", CommandStrategy.class);
    final CommandStrategy sessionSupportedCommandStrategy = beanFactory.getBean(
        "sessionSupportedCommandStrategy", CommandStrategy.class);

    insertStrategies = Map.of(
        SIMPLE, simpleStrategy::save,
        JPA, jpaStrategy::save,
        CACHED, cachedConnectionsStrategy::save,
        SESSION, sessionSupportedCommandStrategy::save);
    updateStrategies = Map.of(
        SIMPLE, simpleStrategy::update,
        JPA, jpaStrategy::update,
        CACHED, cachedConnectionsStrategy::update,
        SESSION, sessionSupportedCommandStrategy::update);
    singleSelectStrategies = Map.of(
        SIMPLE, simpleStrategy::find,
        JPA, jpaStrategy::find,
        CACHED, cachedConnectionsStrategy::find,
        SESSION, sessionSupportedCommandStrategy::find);
    multiSelectStrategies = Map.of(
        SIMPLE, simpleStrategy::findAll,
        JPA, jpaStrategy::findAll,
        CACHED, cachedConnectionsStrategy::findAll,
        SESSION, sessionSupportedCommandStrategy::findAll);
    lockStrategies = Map.of(
        SIMPLE, simpleStrategy::lock,
        JPA, jpaStrategy::lock,
        CACHED, cachedConnectionsStrategy::lock,
        SESSION, sessionSupportedCommandStrategy::lock);
  }

}
