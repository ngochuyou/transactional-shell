/**
 * Wincal-X Project PipelineCommands.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command;

import java.math.BigInteger;
import java.sql.SQLException;
import java.time.Duration;
import java.time.Instant;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import transaction.command.strategy.impl.SessionSupportedCommandStrategy;
import transaction.domain.Timeline;
import transaction.session.Session;

/**
 * PipelineCommands
 */
@ShellComponent
public class MockCommands {

  private static final AtomicReference<BigInteger> LAST_ID = new AtomicReference<>(null);

  private final Session session;
  private final SessionSupportedCommandStrategy sessionSupportedCommandStrategy;

  public MockCommands(Session session,
      SessionSupportedCommandStrategy sessionSupportedCommandStrategy) {
    this.session = session;
    this.sessionSupportedCommandStrategy = sessionSupportedCommandStrategy;
  }

  @ShellMethod(key = "mock")
  public String mock() throws SQLException {
    session.open();
    session.begin(2);

    Instant cursor = Instant.from(OffsetDateTime.of(2019, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC));

    while (cursor.isBefore(Instant.now())) {
      LAST_ID.set(create(LAST_ID.get(), cursor));
      cursor = cursor.plus(Duration.ofHours(6));
    }

    session.commit();
    session.close();

    return "Mocking completed";
  }

  private BigInteger createIn(Instant timestamp) {
    final Timeline newTimeline = new Timeline();

    newTimeline.setUsername("ngochuy.ou");
    newTimeline.setOutTimestamp(null);
    newTimeline.setInTimestamp(timestamp);

    return sessionSupportedCommandStrategy.save(newTimeline).getId();
  }

  private BigInteger createOut(BigInteger existingId, Instant timestamp) {
    final Timeline existingTimeline = sessionSupportedCommandStrategy.find(existingId,
        Timeline.class);

    existingTimeline.setOutTimestamp(timestamp);
    sessionSupportedCommandStrategy.update(existingTimeline);

    return null;
  }

  private BigInteger create(BigInteger existingId, Instant timestamp) {
    if (Objects.isNull(existingId)) {
      return createIn(timestamp);
    }

    return createOut(existingId, timestamp);
  }

}
