/**
 * Wincal-X Project PipelineCommands.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.command;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.core.env.Environment;
import org.springframework.shell.Input;
import org.springframework.shell.InputProvider;
import org.springframework.shell.Shell;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import transaction.declaration.Declarations;
import transaction.shell.CustomShell;

/**
 * PipelineCommands
 */
@ShellComponent
public class PipelineCommands {

  private static final Logger LOGGER = LoggerFactory.getLogger(PipelineCommands.class);
  private static final String COMMAND_DELIMITER = "\\s*;\\s*";

  private final AutowireCapableBeanFactory beanFactory;
  private final Environment env;

  public PipelineCommands(AutowireCapableBeanFactory beanFactory, Environment env) {
    this.beanFactory = beanFactory;
    this.env = env;
  }

  @ShellMethod
  public String pipe(@ShellOption String command) {
    try {
      return Declarations.of(command)
          .map(this::split)
          .consume(this::log)
          .map(this::input)
          .tryMap(this::run)
          .get();
    } catch (Exception e) {
      throw new IllegalStateException(e);
    }
  }

  @ShellMethod(key = "open")
  public String openConnection() {
    try (final Connection connection = DriverManager.getConnection(
        env.getRequiredProperty("spring.datasource.url"),
        env.getRequiredProperty("spring.datasource.username"),
        env.getRequiredProperty("spring.datasource.password"))) {
      return "Connection opened. Is Transaction active: " + !connection.getAutoCommit();
    } catch (SQLException e) {
      throw new IllegalStateException(e);
    }
  }

  private List<String> split(String commandPipe) {
    return Stream.of(commandPipe.split(COMMAND_DELIMITER)).toList();
  }

  private void log(List<String> commands) {
    if (LOGGER.isDebugEnabled()) {
      LOGGER.debug("\nExecuting pipeline: \n\t{}", String.join("\n\t", commands));
    }
  }

  private List<InputProvider> input(List<String> commands) {
    return commands.stream()
        .map(commandString -> (InputProvider) () -> (Input) () -> commandString)
        .toList();
  }

  private String run(List<InputProvider> commands) throws Exception {
    final Shell customShell = beanFactory.createBean(CustomShell.class);

    for (final InputProvider command : commands) {
      customShell.run(command);
    }

    return "Pipeline executed successfully";
  }

}
