/**
 * 
 */
package transaction.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledFunction<FIRST, RETURN, EXCEPTION extends Exception> {

	RETURN apply(FIRST input) throws EXCEPTION;

	static <F, E extends Exception> HandledFunction<F, F, E> identity() {
		return first -> first;
	}

}
