/**
 * 
 */
package transaction.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledTriFunction<FIRST, SECOND, THIRD, RETURN, EXCEPTION extends Exception> {

	RETURN apply(FIRST fisrt, SECOND second, THIRD third) throws EXCEPTION;

}
