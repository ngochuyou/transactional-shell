/**
 * 
 */
package transaction.function;

/**
 * @author Ngoc Huy
 *
 */
@FunctionalInterface
public interface HandledTriConsumer<FIRST, SECOND, THIRD, EXCEPTION extends Exception> {

	void accept(FIRST first, SECOND second, THIRD third) throws EXCEPTION;

}
