/**
 *
 */
package transaction.declaration;

import java.util.function.Consumer;
import java.util.function.Function;
import transaction.function.HandledConsumer;
import transaction.function.HandledFunction;

/**
 * @author Ngoc Huy
 *
 */
public class DeclarationImpl<FIRST> implements Declaration<FIRST> {

  protected final FIRST first;

  DeclarationImpl(FIRST first) {
    this.first = first;
  }

  @Override
  public FIRST get() {
    return first;
  }

  @Override
  public <RETURN> Declaration<RETURN> map(Function<FIRST, RETURN> mapper) {
    return new DeclarationImpl<>(mapper.apply(first));
  }

  @Override
  public Declaration<FIRST> consume(Consumer<FIRST> consumer) {
    consumer.accept(first);
    return this;
  }

  @Override
  public <RETURN, E extends Exception> Declaration<RETURN> tryMap(
      HandledFunction<FIRST, RETURN, E> mapper) throws E {
    return new DeclarationImpl<>(mapper.apply(first));
  }

  @Override
  public <E extends Exception> Declaration<FIRST> tryConsume(HandledConsumer<FIRST, E> consumer)
      throws E {
    consumer.accept(first);
    return this;
  }

  @Override
  public <SECOND> BiDeclaration<FIRST, SECOND> second(SECOND second) {
    return new BiDeclarationImpl<>(first, second);
  }

}
