/**
 *
 */
package transaction.declaration;

import java.util.function.Consumer;
import java.util.function.Function;
import transaction.function.HandledConsumer;
import transaction.function.HandledFunction;

/**
 * @author Ngoc Huy
 *
 */
public interface Declaration<FIRST> {

  FIRST get();

  <RETURN> Declaration<RETURN> map(Function<FIRST, RETURN> mapper);

  Declaration<FIRST> consume(Consumer<FIRST> consumer);

  <RETURN, E extends Exception> Declaration<RETURN> tryMap(HandledFunction<FIRST, RETURN, E> mapper)
      throws E;

  <E extends Exception> Declaration<FIRST> tryConsume(HandledConsumer<FIRST, E> consumer) throws E;

  <SECOND> BiDeclaration<FIRST, SECOND> second(SECOND second);

}
