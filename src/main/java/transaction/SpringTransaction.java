package transaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication(exclude = HibernateJpaAutoConfiguration.class)
@EnableTransactionManagement
@EnableSpringDataWebSupport
public class SpringTransaction {

  public static void main(String[] args) {
    SpringApplication.run(SpringTransaction.class);
  }

//  public static void main(String[] args) throws ClassNotFoundException {
//    DriverManager.setLogWriter(new PrintWriter(System.out));
//    Class.forName("com.mysql.cj.jdbc.Driver");
//  }

//  public static void main(String[] args) throws SQLException {
//    DriverManager.setLogWriter(new PrintWriter(System.out));
//    new Driver();
//  }
}