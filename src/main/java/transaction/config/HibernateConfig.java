/**
 * Wincal-X Project HibernateConfig.java Copyright © ALMEX Inc. All rights reserved.
 */
package transaction.config;

import com.zaxxer.hikari.HikariDataSource;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.DriverManager;
import java.util.Properties;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.hibernate.boot.model.naming.CamelCaseToUnderscoresNamingStrategy;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.TransactionManager;

/**
 * HibernateConfig
 */
@Configuration
public class HibernateConfig {

  static {
    DriverManager.setLogWriter(new PrintWriter(System.out));
  }

  @Bean
  public DataSource dataSource(
      @Value("${spring.jpa.datasource}") String dataSourceClassPath,
      Environment env)
      throws ClassNotFoundException, NoSuchMethodException,
      InvocationTargetException, InstantiationException, IllegalAccessException {
    final DatasourceProvider datasourceProvider = (DatasourceProvider) Class.forName(
            dataSourceClassPath)
        .getConstructor()
        .newInstance();

    return datasourceProvider.get(env);
  }

  @Bean(name = "entityManagerFactory")
  public FactoryBean<SessionFactory> sessionFactory(
      DataSource dataSource,
      @Value("${spring.jpa.properties[hibernate.dialect]}") String dialect) {
    final LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();

    sessionFactory.setDataSource(dataSource);
    sessionFactory.setPackagesToScan("transaction");
    // snake_case for columns
    sessionFactory.setPhysicalNamingStrategy(new CamelCaseToUnderscoresNamingStrategy());

    final Properties properties = new Properties();

    properties.put(AvailableSettings.DIALECT, dialect);
    properties.put(AvailableSettings.SHOW_SQL, true);
    properties.put(AvailableSettings.FORMAT_SQL, true);
    properties.put(AvailableSettings.HBM2DDL_AUTO, "update");
    properties.put(AvailableSettings.STATEMENT_BATCH_SIZE, 50);
    properties.put(AvailableSettings.ORDER_INSERTS, true);
    properties.put(AvailableSettings.ORDER_UPDATES, true);

    sessionFactory.setHibernateProperties(properties);

    return sessionFactory;
  }

  @Bean
  public TransactionManager transactionManager(SessionFactory sessionFactory) {
    return new HibernateTransactionManager(sessionFactory);
  }

  public interface DatasourceProvider {

    DataSource get(Environment env);

  }

  private static abstract class AbstractDataSourceProvider implements DatasourceProvider {

    private String get(Environment env, String propName) {
      return env.getRequiredProperty(propName);
    }

    protected String getDriverClassName(Environment env) {
      return get(env, "spring.datasource.driver-class-name");
    }

    protected String getUrl(Environment env) {
      return get(env, "spring.datasource.url");
    }

    protected String getUsername(Environment env) {
      return get(env, "spring.datasource.username");
    }

    protected String getPassword(Environment env) {
      return get(env, "spring.datasource.password");
    }
  }

  @SuppressWarnings("unused")
  public static class HikariDatasourceProvider extends AbstractDataSourceProvider {

    @Override
    public DataSource get(Environment env) {
      final HikariDataSource dataSource = new HikariDataSource();

      dataSource.setDriverClassName(getDriverClassName(env));
      dataSource.setJdbcUrl(getUrl(env));
      dataSource.setUsername(getUsername(env));
      dataSource.setPassword(getPassword(env));

      return dataSource;
    }
  }

  @SuppressWarnings("unused")
  public static class DriverDatasourceProvider extends AbstractDataSourceProvider {

    @Override
    public DataSource get(Environment env) {
      final DriverManagerDataSource dataSource = new DriverManagerDataSource();

      dataSource.setDriverClassName(getDriverClassName(env));
      dataSource.setUrl(getUrl(env));
      dataSource.setUsername(getUsername(env));
      dataSource.setPassword(getPassword(env));

      return dataSource;
    }
  }

}
